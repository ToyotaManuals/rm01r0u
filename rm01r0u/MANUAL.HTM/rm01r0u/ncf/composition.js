var maptitle = new Array("New Car Features");

var tree = new Array(
"Second Hierarchy","Second HierarchyKey","Third Hierarchy","Third HierarchyKey","Fourth Hierarchy","PDF Files Name",
"INTRODUCTION","1","OUTLINE OF NEW FEATURES","1","OUTLINE OF NEW FEATURES","m_in_0002.pdf",
"","1","EXTERIOR APPEARANCE","2","EXTERIOR APPEARANCE","m_in_0006.pdf",
"","1","MODEL CODE","3","MODEL CODE","m_in_0007.pdf",
"","1","MODEL LINE-UP","4","MODEL LINE-UP","m_in_0007.pdf",
"NEW FEATURES","2","EXTERIOR","5","HEADLIGHT","m_nf_0010.pdf",
"","2","","5","RADIATOR GRILLE","m_nf_0010.pdf",
"","2","","5","FRONT SPOILER","m_nf_0011.pdf",
"","2","","5","REAR COMBINATION LIGHT","m_nf_0011.pdf",
"","2","1NZ-FXE ENGINE","6","ENGINE CONTROL SYSTEM","m_nf_0012.pdf",
"","2","TIRE PRESSURE WARNING SYSTEM","7","DESCRIPTION","m_nf_0030.pdf",
"","2","","7","CONSTRUCTION AND OPERATION","m_nf_0030.pdf",
"","2","SRS AIRBAG SYSTEM","8","FRONT PASSENGER OCCUPANT CLASSIFICATION SYSTEM","m_nf_0036.pdf",
"","2","","8","DIAGNOSIS","m_nf_0044.pdf",
"","2","TOYOTA PARKING ASSIST SYSTEM","9","REAR VIEW MONITOR SYSTEM","m_nf_0046.pdf",
"APPENDIX","3","MAJOR TECHNICAL SPECIFICATIONS","10","MAJOR TECHNICAL SPECIFICATIONS","m_ap_0052.pdf"
);

